module.exports = function(app) {

	// server routes ===========================================================
	// handle things like api calls
	// authentication routes

	// frontend routes =========================================================
	// route to handle all angular requests


    app.get('/test', function(req, res) {

        var dummyData = {
            "names": ["Talha", "Ivan"]
        }
        res.send(dummyData)
    });


    app.post('/test', function(req, res) {
        console.log(req.body)
        res.send({"msg": "Received"})
    });


	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});


};