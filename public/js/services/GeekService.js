
angular.module('GeekService', []).factory('Geek', ['$http', function($http) {

    return {
        test: function (success, error) {
            $http.get('/test').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },
        createTest: function (credentials, success, error) {
            $http.post('/test', credentials, {} ).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);